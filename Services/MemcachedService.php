<?php

namespace Nixdo\MemcachedBundle\Services;

use Nixdo\CacheBundle\Interfaces\CacheSystemInterface;

class MemcachedService implements CacheSystemInterface {

    protected $loaded = false;
    protected $memcached;
    protected $serverAddr;
    protected $serverPort;
    protected $persistantId;

    public function add($key, $value) {
        return $this->memcached->add($key, $value);
    }

    public function set($key, $value) {
        return $this->memcached->set($key, $value);
    }

    public function get($key) {
        return $this->memcached->get($key);
    }

    public function getAll() {
        return $this->memcached->getAllKeys();
    }

    public function delete($key) {
        return $this->memcached->delete($key);
    }

    public function isLoaded() {
        return $this->loaded;
    }

    public function getMemcached() {
        return $this->memcached;
    }

    public function __construct($addr, $port, $persistantId) {
        $this->serverAddr = $addr;
        $this->serverPort = $port;
        $this->persistantId = $persistantId;
        if (class_exists('Memcached') === true) {
            $this->memcached = new \Memcached($this->persistantId);
            $servers = $this->memcached->getServerList();
            $serverAlreadyRegistered = false;
            foreach ($servers as $server) {
                if ($server['host'] == $this->serverAddr) {
                    $serverAlreadyRegistered = true;
                }
            }
            if ($serverAlreadyRegistered === false) {
                $this->memcached->addServer($this->serverAddr, $this->serverPort);
            }
            $this->loaded = true;
        } else {
            $this->loaded = false;
        }
    }

}
